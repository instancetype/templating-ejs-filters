/**
 * Created by instancetype on 6/20/14.
 */
var ejs = require('ejs')
  , template = '<%=: movies | last %>'// first, get:0, get:<property>
  , context = {'movies': [ 'The Shining'
                         , 'Jaws'
                         , 'Psycho'
                         ]}

console.log(ejs.render(template, context))