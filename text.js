/**
 * Created by instancetype on 6/20/14.
 */
var ejs = require('ejs')
  , template = '<%=: title | truncate: 8 %>'// truncate_words:2, append:'some text', prepend:'some text'
  , context = {title: 'The Once and Future King'}

console.log(ejs.render(template, context))

var template2 = "<%=: title | replace:'Future', 'Past' | replace:'Once', 'Twice' %>" // Also accepts regexp

console.log(ejs.render(template2, context))