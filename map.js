/**
 * Created by instancetype on 6/20/14.
 */
var ejs = require('ejs')
  , template = "<%=: movies | map:'name' | sort | first %>"
  , context = {'movies': [ {name: 'Psycho'}
    , {name: 'Jaws'}
    , {name: 'The Shining'}
  ]}

console.log(ejs.render(template, context))