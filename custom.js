/**
 * Created by instancetype on 6/20/14.
 */
var ejs = require('ejs')
  , template = '<%=: price * 1.14577 | round:2 %>'
  , context = {price: 103}

ejs.filters.round = function(number, decimalPlaces) {
  number = isNaN(number)
         ? 0
         : number

  decimalPlaces = !decimalPlaces
                ? 0
                : decimalPlaces

  var multiple = Math.pow(10, decimalPlaces)
  return Math.round(number * multiple) / multiple
}

console.log(ejs.render(template, context))